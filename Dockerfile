# Use an official Java image as the base image
FROM openjdk:17-jdk-slim-buster

RUN ls

# Copy the source code to the container
COPY . /app

RUN ls

# Set the working directory in the container to /app
WORKDIR /app

RUN ls

# Copy the jar file from the host to the container
COPY target/*.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","app.jar"]

